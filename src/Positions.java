import java.util.ArrayList;
import java.util.List;

/**
  * Positions enregistre toutes les positions, quelque soit le lot.
  *
  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
  */
public class Positions extends PositionsAbstrait {

	// TODO à faire...
	
	/**
	  * PositionsAbstrait spécifie un traitement qui mémorise
	  * toutes les positions traitées pour ensuite y accéder
	  * par leur indice ou obtenir la fréquence d'une position.
	  *
	  * @author	Xavier Crégut <Prenom.Nom@enseeiht.fr>
	  */

	

		private ArrayList<Position> listDePosition = new ArrayList<Position>();

		/** Obtenir le nombre de positions mémorisées.
		 * @return le nombre de positions mémorisées
		 */
		public int nombre() {
			
			return listDePosition.size();
		}

		/** Obtenir la ième position enregistrée.
		 * @param numero numéro de la position souhaitée (0 pour la première)
		 * @return la position de numéro d'ordre `numero`
		 * @exception IndexOutOfBoundsException si le numero est incorrect
		 */
		public  Position position(int indice) {
			return listDePosition.get(indice);
			
		}

		/** Obtenir la fréquence d'une position dans les positions traitées.
		 * @param position la position dont on veut connaître la fréquence
		 * @return la fréquence de la position en paramètre
		 */
		public int frequence(Position position) {
			int nombreOccurence = 0;
			for (Position element : listDePosition) {
				if(element.equals(position)) {
					nombreOccurence++;
				}
			}
			return nombreOccurence;
		}
	
	public void traiter(Position position, double valeur) {
		listDePosition.add(position);
		for (Traitement suivant : this.suivants) {
			suivant.traiter(position, valeur);
		}
	}
}
