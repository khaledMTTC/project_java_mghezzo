
import java.util.ArrayList;

public class CycleException extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	ArrayList<Traitement> listDesTraitement = new ArrayList<Traitement>();

	public CycleException() {
	}

	public void checkCycle(Traitement traitement) throws Throwable {
		if (listDesTraitement.isEmpty()) {
			listDesTraitement.add(traitement);
		} else if (!listDesTraitement.contains(traitement)) {
			listDesTraitement.add(traitement);
		} else {
			System.out.println("Cycle existe :  le traitement "+traitement.getClass().getName()+ " existe plusieurs fois dans la chaine avec un seul instance");
			throw new CycleException();
		}
		for (Traitement trait : traitement.suivants) {
			checkCycle(trait);
		}
		
	}
}