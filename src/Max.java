/**
 * Max calcule le max des valeurs vues, quelque soit le lot.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */

public class Max extends Traitement {

	// TODO à faire...

	private double max = Double.NEGATIVE_INFINITY;
	private double min = Double.POSITIVE_INFINITY;

	public void traiter(Position position, double valeur) {
		this.max = Math.max(this.max, valeur);
		this.min = Math.min(this.min, valeur);
		for (Traitement suivant : this.suivants) {
			suivant.traiter(position, valeur);
		}
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = Math.max(this.max, max);
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = Math.min(this.min, min);
	}

	protected void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + " : max = " + this.max);
	}

}
