import java.io.FileNotFoundException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import org.jdom2.*;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * GenerateurXML écrit dans un fichier, à charque fin de lot, toutes les données
 * lues en indiquant le lot dans le fichier XML.
 *
 * @author Xavier Crégut <Prenom.Nom@enseeiht.fr>
 */
public class GenerateurXML extends Traitement {

	// TODO à faire...

	private String nomFichier;
	private ArrayList<Integer> x;
	private ArrayList<Integer> y;
	private ArrayList<Double> valeur;

	public GenerateurXML(String nomString) {
		this.nomFichier = nomString;
		this.x = new ArrayList<Integer>();
		this.y = new ArrayList<Integer>();
		this.valeur = new ArrayList<Double>();
	}

	public void traiter(Position position, double valeur) {
		x.add(position.x);
		y.add(position.y);
		this.valeur.add(valeur);
		for (Traitement suivant : this.suivants) {
			suivant.traiter(position, valeur);
		}
	}

	protected void gererFinLotLocal(String nomLot) {
		generFichierXml();

	}
	
	protected String toStringComplement() {
		return "\" " + this.nomFichier + " \"";
	}
	
	
	private void generFichierXml() {
		// TODO Auto-generated method stub
		Element racine = new Element("donnees");
		ArrayList<Element> donnees = new ArrayList<Element>();
		for (int i = 0; i < x.size(); i++) {
			Element donnee = new Element("donnee");
			//rajout les attributs
			donnee.setAttribute("id", ((Integer) (i + 1)).toString());
			donnee.setAttribute("y", this.y.get(i).toString());
			// rajout les elements
			donnee.addContent(new Element("valeur").setText(this.valeur.get(i).toString()));
			donnee.addContent(new Element("x").setText(this.x.get(i).toString()));
			donnees.add(donnee);
		}
		racine.addContent(donnees);
		Document document = new Document(racine, new DocType("Donnees", "donnees7.dtd"));
		XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
		try {
			OutputStream fichier = new FileOutputStream(this.nomFichier);

			sortie.output(document, fichier);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
