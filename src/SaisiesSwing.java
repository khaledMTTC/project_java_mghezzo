

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.*;

public class SaisiesSwing {
	private String nomFichier;
	JTextField tAbscisse = new JTextField("");
	JTextField tOrdonnee = new JTextField("");
	JTextField tValeur = new JTextField("");
	List<Integer> x = new ArrayList<>();
	List<Integer> y = new ArrayList<>();
	List<Double> valeur = new ArrayList<>();

	public SaisiesSwing(String nomfichier) {
		nomFichier = nomfichier;
		JFrame fenetre = new JFrame();// pour fenetre 
		fenetre.setTitle("Saisie données");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// pour rajoute X

		Container contenu = fenetre.getContentPane();
		contenu.setLayout(new BorderLayout());

		JPanel pAbscisse = new JPanel(new BorderLayout());

		tAbscisse.setColumns(10);
		JLabel lAbscisse = new JLabel("Abscisse");
		lAbscisse.setHorizontalAlignment(JLabel.CENTER);
		pAbscisse.add(lAbscisse, BorderLayout.CENTER);
		pAbscisse.add(tAbscisse, BorderLayout.SOUTH);

		JPanel pOrdonnee = new JPanel(new BorderLayout());

		tOrdonnee.setColumns(10);
		JLabel lOrdonnee = new JLabel("Ordonnée");
		lOrdonnee.setHorizontalAlignment(JLabel.CENTER);
		pOrdonnee.add(lOrdonnee, BorderLayout.CENTER);
		pOrdonnee.add(tOrdonnee, BorderLayout.SOUTH);

		JPanel pValeur = new JPanel(new BorderLayout());

		tValeur.setColumns(10);
		JLabel lValeur = new JLabel("Valeur");
		lValeur.setHorizontalAlignment(JLabel.CENTER);
		pValeur.add(lValeur, BorderLayout.NORTH);
		pValeur.add(tValeur, BorderLayout.SOUTH);

		JPanel textSaisie = new JPanel(new FlowLayout());
		textSaisie.add(pAbscisse);
		textSaisie.add(pOrdonnee);
		textSaisie.add(pValeur);

		contenu.add(textSaisie, BorderLayout.NORTH);

		JButton bValide = new JButton("Valider");
		JButton bEffacer = new JButton("Effacer");
		JButton bTerminer = new JButton("Terminer");

		JPanel buttons = new JPanel(new FlowLayout());
		contenu.add(buttons, BorderLayout.SOUTH);

		buttons.add(bValide);
		buttons.add(bEffacer);
		buttons.add(bTerminer);

		bTerminer.addActionListener(new ActionTerminer());
		bEffacer.addActionListener(new ActionEffacer());
		bValide.addActionListener(new ActionValider());
		tAbscisse.addFocusListener(new FocusSetBackground());
		tOrdonnee.addFocusListener(new FocusSetBackground());
		tValeur.addFocusListener(new FocusSetBackground());
		

		fenetre.pack();
		fenetre.setVisible(true);

	}

	
	class FocusSetBackground implements FocusListener {

		@Override
		public void focusGained(FocusEvent e) {
			if (e.getSource() == tAbscisse) {
				tAbscisse.setBackground(Color.WHITE);
				tAbscisse.selectAll();
			}
			if (e.getSource() == tOrdonnee) {
				tOrdonnee.setBackground(Color.WHITE);
				tOrdonnee.selectAll();
			}
			if (e.getSource() == tValeur) {
				tValeur.setBackground(Color.WHITE);
				tValeur.selectAll();
			}
		}

		@Override
		public void focusLost(FocusEvent e) {
			/*
			 * *********************************************** on fair rien ici , on a pas
			 * besion de préciser l'evenement apres le focus est perdu
			 * ************************************************
			 */
		}
	}

	final class ActionTerminer implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent ev) {
			try {
				// attach a file to FileWriter
				FileWriter fw = new FileWriter(nomFichier);

				// read character wise from string and write
				// into FileWriter
				for (int i = 0; i < x.size(); i++)
					fw.write(x.get(i) + " " + y.get(i) + " " + (i + 1) + " " + valeur.get(i) + "\n");

				System.out.println("Writing successful");
				// close the file
				fw.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			System.exit(0);
		}
	}

	class ActionEffacer implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent ev) {
			tAbscisse.setText("");
			tOrdonnee.setText("");
			tValeur.setText("");
			tAbscisse.setBackground(Color.WHITE);
			tOrdonnee.setBackground(Color.WHITE);
			tValeur.setBackground(Color.WHITE);

		}
	}

	final class ActionValider implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent ev) {

			String sAbscisse = tAbscisse.getText();
			String sOrdonnee = tOrdonnee.getText();
			String sValeur = tValeur.getText();

			Integer iAbscisse = 0;
			Integer iOrdonnee = 0;
			Double dValeur = 0.0;

			boolean bAbscisse = false, bOrdonnee = false, bValeur = false;

			try {
				iAbscisse = Integer.parseInt(sAbscisse);
				bAbscisse = true;

			} catch (NumberFormatException e) {
				tAbscisse.setBackground(Color.RED);
			}

			try {
				iOrdonnee = Integer.parseInt(sOrdonnee);
				bOrdonnee = true;

			} catch (NumberFormatException e) {
				tOrdonnee.setBackground(Color.RED);
			}
			try {
				dValeur = Double.parseDouble(sValeur);
				bValeur = true;
			} catch (NumberFormatException e) {
				tValeur.setBackground(Color.RED);
			}
			if (bAbscisse && bOrdonnee && bValeur) {
				x.add(iAbscisse);
				y.add(iOrdonnee);
				valeur.add(dValeur);
			}

		}
	}

	public static void main(String[] args) {
		Scanner fileObj = new Scanner(System.in); // Create a Scanner object for File
		System.out.println("You are about to start a Graphical Interface to enter data to be traited later :");
		System.out.println("Please enter file's name in which you will save data into :");
		String fileName = fileObj.nextLine();
		fileObj.close();
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				new SaisiesSwing(fileName);

			}
		});

	}
}


